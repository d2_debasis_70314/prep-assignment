#include <stdio.h>
#include <strings.h>

int main()
{
    int i, counter[26];
    char str[255];
    printf("Enter a name:");
    scanf("%[^\n]", str);
    for ( i = 0; i < 26; i++) counter[i] = 0;
    for ( i = 0; i < strlen(str); i++)
        if     (str[i] >= 65 && str[i] <= 90) counter[(int)str[i]-65]++;
        else if(str[i] >= 97 && str[i] <= 122) counter[(int)str[i]-97]++;
    for ( i = 0; i < 26; i++) if(counter[i] != 0) printf("%c:%d\n", 65+i, counter[i]);
}