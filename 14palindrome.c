#include <stdio.h>
#include <string.h>

int main()
{
    int i, j, flag = 0;
    char str[20];
    printf("Enter a name:");
    scanf("%[^\n]", str);
    i = 0;
    j = strlen(str);
    while (i < j)
    {
        if (str[i] == str[j])
        {
            flag = 1;
            break;
        }
        i++;
        j--;
    }
    if (flag == 0) printf("The given string is a palindrome");
    else           printf("The given string is not a palindrome");
}