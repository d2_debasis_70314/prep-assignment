#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cmp(const void *p, const void *q);

int cmp(const void *p, const void *q) 
{
    const char *p1 = p;
    const char *q1 = q;

    return strcmp(p1,q1);
}

int main()
{
    int i;
    char nameOfStudents[10][50];
    for ( i = 0; i < 10; i++)
    {
        printf("Enter a name:");
        scanf("%s", nameOfStudents[i]);
    }
    qsort(nameOfStudents, 10, 50*sizeof(char), cmp);
     printf("Sorted names:\n");
    for ( i = 0; i < 10; i++)
        printf("%s\n", nameOfStudents[i]);
}