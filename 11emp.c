#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Employee
{
    char fname[20];
    char lname[20];
    float salary;
};

int main()
{
    struct Employee emp1;
    printf("Enter first name: ");
    scanf("%s", emp1.fname);
    printf("Enter last name: ");
    scanf("%s", emp1.lname);
    printf("Enter salary: ");
    scanf("%f", &emp1.salary);

    struct Employee emp2;
    printf("Enter first name: ");
    scanf("%s", emp2.fname);
    printf("Enter last name: ");
    scanf("%s", emp2.lname);
    printf("Enter salary: ");
    scanf("%f", &emp2.salary);

    printf("Salary of %s %s: %.2f\n", emp1.fname, emp1.lname, emp1.salary);
    printf("Salary of %s %s: %.2f\n", emp2.fname, emp2.lname, emp2.salary);

    printf("Raise given...\n");
    printf("Salary of %s %s: %.2f\n", emp1.fname, emp1.lname, 1.10*emp1.salary);
    printf("Salary of %s %s: %.2f\n", emp2.fname, emp2.lname, 1.10*emp2.salary);
}