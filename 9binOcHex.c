#include <stdio.h>
#include <math.h>

int main()
{
    int b, x, y, l, r, i;
    printf("Enter a number:");
    scanf("%d", &x);
    //to binary
    b = 2;
    y = x;
    l = (int) ceil(log(1.0*y) / log(1.0*b));
    i = 0;
    char conv[l];
    while(y != 0)
    {
        r = y % b;
        y = (int)(1.0*y / b);
        conv[i++] = r + '0';
    }
    printf("%d in Binary: ", x);
    for ( i = l-1; i >= 0; i--)
        printf("%c", conv[i]);
    printf("\n");
    //to octal
    b = 8;
    y = x;
    l = (int) ceil(log(1.0*y) / log(1.0*b));
    i = 0;
    char conv1[l];
    while(y != 0)
    {
        r = y % b;
        y = (int)(1.0*y / b);
        conv1[i++] = r + '0';
    }
    printf("%d in Octal: ", x);
    for ( i = l-1; i >= 0; i--)
        printf("%c", conv1[i]);
    printf("\n");
    //to hexadecimal
    b = 16;
    y = x;
    l = (int) ceil(log(1.0*y) / log(1.0*b));
    i = 0;
    char conv2[l];
    while(y != 0)
    {
        r = y % b;
        y = (int)(1.0*y / b);
        if (r < 10) conv2[i++] = r + '0';
        else        conv2[i++] = r + 55;
    }
    printf("%d in Hexadecimal: ", x);
    for ( i = l-1; i >= 0; i--)
        printf("%c", conv2[i]);
    printf("\n");
}