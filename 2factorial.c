#include <stdio.h>

int main()
{
    int i, m=1, n;
    printf("Enter the value of n: ");
    scanf("%d", &n);
    for (i = n; i > 0; i--) m *= i;
    printf("%d! = %d", n, m); 
}