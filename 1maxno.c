#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[])
{
    int i, max, temp;
    max = atoi(argv[1]);
    for (i = 2; i <= argc; i++)
    {
        temp = atoi(argv[i]);
        max = (temp > max) ? temp : max;
    }
    printf("Max number: %d", max);
    return 0;
}