#include <stdio.h>
#include <string.h>

int main()
{
    int i ,j, k, n, sum;
    printf("Enter dimension of square matrix:\n");
    scanf("%d", &n);
    int arr1[n][n], arr2[n][n], arr3[n][n];
    printf("Enter elements of first matrix:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Enter arr1[%d][%d]:", i, j);
            scanf("%d", &arr1[i][j]);
        }
    }
     printf("Enter elements of second matrix:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            printf("Enter arr2[%d][%d]:", i, j);
            scanf("%d", &arr2[i][j]);
        }
    }
    printf("arr1:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
            printf("%d ",arr1[i][j]);
        printf("\n");
    }
    printf("arr2:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
            printf("%d ",arr2[i][j]);
        printf("\n");
    }
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            arr3[i][j] = 0;
            for (k = 0; k < n; k++) 
                arr3[i][j] += arr1[i][k]*arr2[k][j];
        }
    }
     printf("arr3:\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
            printf("%d ",arr3[i][j]);
        printf("\n");
    }
}