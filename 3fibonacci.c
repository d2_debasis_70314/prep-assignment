#include <stdio.h>

int main()
{
    int n, Fn0=0, Fn1=1, temp;
    printf("Enter the value of n: ");
    scanf("%d", &n);
    printf("First n fibonacci numbers:\n");
    printf("%d %d ", Fn0, Fn1);
    n = n-2;
    while(n-- > 0)
    {
        temp = Fn1 + Fn0;
        Fn0 = Fn1;
        Fn1 = temp; 
        printf("%d ", Fn1);
    }
}