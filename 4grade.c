#include <stdio.h>

int main()
{
    int marks, i;
    char subject[20];
    for (i = 0; i < 5; i++)
    {
        printf("Enter subject: ");
        scanf("%s", subject);

        printf("Enter marks in %s: ", subject);
        scanf("%d", &marks);

        if      (marks >= 90) printf("Grade: Ex\n");
        else if (marks >= 80) printf("Grade: A\n");
        else if (marks >= 70) printf("Grade: B\n");
        else if (marks >= 60) printf("Grade: C\n");
        else                  printf("Grade: F\n");
    }
    
}