#include <stdio.h>
#include <string.h>

int main()
{
    char str[255];
    int i, upper=0, lower=0, number=0, other=0;
    printf("Enter characters:\n");
    scanf("%[^\n]", &str);
    for (i = 0; i < strlen(str); i++)
    {
        if      (str[i] >= 'a' && str[i] <= 'z') lower++;
        else if (str[i] >= 'A' && str[i] <= 'Z') upper++;
        else if (str[i] >= '0' && str[i] <= '9') number++;
        else                                     other++;
    }
    printf("Lowercase characters: %d\n", lower);
    printf("Uppercase characters: %d\n", upper);
    printf("Number characters:    %d\n", number);
    printf("Other characters:     %d\n", other);
}