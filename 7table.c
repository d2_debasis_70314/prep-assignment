#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int n, i, j=10;
    n = atoi(argv[1]);
    printf("Table of %d:\n", n);
    for (i = 1; i <= j; i++)
        printf("%d x %d = %d\n", n, i, n*i);
}